import 'package:flutter/material.dart';

class StatusRoute extends StatefulWidget {
  StatusRoute({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _StatusRouteState createState() => _StatusRouteState();
}

class _StatusRouteState extends State<StatusRoute> with WidgetsBindingObserver {
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          SingleChildScrollView(
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                            '⚠️ This app is work in progress. Some or all features may not be available or wrongly documented.',
                            style: Theme
                                .of(context)
                                .textTheme
                                .headline),
                        Divider(color: Colors.black),
                        Center(
                            child: Text('✔️No alerts!',
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .headline)),
                        Center(
                            child: Text(
                                'WORK IN PROGRESS, REAL STATUS NOT SHOWN',
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .headline)),
                        Center(
                            child: Text(
                                'Swipe right and left to see other options.')),
              ]
            )
          )
        )
      ],
      )
    );
  }
}
