import 'package:flutter/material.dart';

class QaaRoute extends StatefulWidget {
  QaaRoute({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _QaaRouteState createState() => _QaaRouteState();
}

class _QaaRouteState extends State<QaaRoute> with WidgetsBindingObserver {
  Widget build(BuildContext context) {
    return Center(
      child: Column(
      children: <Widget>[
        SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  '⚠️ This app is work in progress. Some or all features may not be available or wrongly documented.',
                  style: Theme.of(context).textTheme.headline),
                Divider(color: Colors.black),
                Text('Q: Does Nearlog track/use my location?'),
                Text(
                  'A: Nearlog Nearlog does not track/use/save your location. The data that is stored is an identifier (UUID v4) that is unique to your device and changed by pressing the "GenSave" button and a list of what devices this device was close to for up to one month.'),
                Divider(color: Colors.black),
                Text('Q: How does Nearlog work?'),
                Text(
                  'A: Nearlog works by setting up a "beacon" which emits using Bluetooth. The beacon emits an identifier that can be changed to nearby devices, and nearby device that havethe Nearlog app will do too. Nearlog also records which devices were nearby, and looks up in a central database on the Internet if any identifier in its history is marked as "infected". If it is in the list, Nearlog sends you an alert and also uploads this device\'s identifier as "infected" until you check with health officials that you are not infected.'),
                Divider(color: Colors.black),
                Text(
                  'Q: How do I report that I tested positive or not?'),
                Text(
                  'A: If you tested positive, go to the "Report" section of the app and tap the button that says "I tested positive". Then, select the disease (e.g. COVID-19), and press "Report". Your anynymous identifier will be reported to a central database so others can see if they contacted an infected person.'),
                Text(
                  'A: If you tested negative and your anonymous identifier was marked as "infected", go to the "Report" section of the app and tap the button that says "I tested negative". Then, select the disease and upload any necessary documents to prove that you tested negative.')
              ]
            )
          )
          )
        ],
      )
    );
  }
}
