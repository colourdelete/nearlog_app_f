//import 'package:flutter/material.dart';
//import 'package:shared_preferences/shared_preferences.dart';
//import 'package:uuid/uuid.dart';
//
//class UUIDRoute extends StatefulWidget {
//  UUIDRoute({Key key, this.title}) : super(key: key);
//  final String title;
//  @override
//  _UUIDRouteState createState() => _UUIDRouteState();
//}
//
//class _UUIDRouteState extends State<UUIDRoute> with WidgetsBindingObserver {
//  var uuid = new Uuid();
//  var _uuid = "not_set";
//
//  _readUuid() async {
//    final prefs = await SharedPreferences.getInstance();
//    final key = 'uuid';
//    final value = prefs.getString(key) ?? 0;
//    setState(() {
//      _uuid = value;
//    });
//    print('read: $value');
//  }
//
//  _saveUuid() async {
//    final prefs = await SharedPreferences.getInstance();
//    final key = 'uuid';
//    final value = '$_uuid';
//    prefs.setString(key, value);
//    print('saved $value');
//    print('uuid is now $_uuid');
//  }
//
//  _genUuid() async {
//    var newValue = uuid.v4();
//    setState(() {
//      _uuid = newValue;
//    });
//  }
//
//  _genSaveUuid() async {
//    await _genUuid();
//    _saveUuid();
//  }
//  Widget build(BuildContext context) {
//    return Scaffold(
//        appBar: AppBar(
//          title: Text('UUID'),
//        ),
//        body: Center(
//          child: Column(
//            //mainAxisAlignment: MainAxisAlignment.center,
//            children: <Widget>[
//              Padding(
//                padding: const EdgeInsets.all(8.0),
//                child: RaisedButton(
//                  child: Text('Read'),
//                  onPressed: () {
//                    _readUuid();
//                  },
//                ),
//              ),
//              Padding(
//                padding: const EdgeInsets.all(8.0),
//                child: RaisedButton(
//                  child: Text('GenSave'),
//                  onPressed: () {
//                    _genSaveUuid();
//                  },
//                ),
//              ),
//              Padding(
//                padding: const EdgeInsets.all(8.0),
//                child: Text('This device\'s UUID is:'),
//              ),
//              Padding(
//                padding: const EdgeInsets.all(8.0),
//                child: Text('$_uuid'),
//              ),
//            ],
//          ),
//        )
//    );
//  }
//}
