import 'package:flutter/material.dart';
import 'package:nearlog_app_f/page_ble.dart';
import 'package:nearlog_app_f/page_history.dart';
import 'package:nearlog_app_f/page_qaa.dart';
import 'package:nearlog_app_f/page_report.dart';
import 'package:nearlog_app_f/page_settings.dart';
import 'package:nearlog_app_f/page_status.dart';

import 'lib_file.dart';

class RootRoute extends StatefulWidget {
  RootRoute({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _RootRouteState createState() => _RootRouteState();
}

class _RootRouteState extends State<RootRoute> with WidgetsBindingObserver {
//  dbOpenDb() async { // DataBaseOPENDataBase
//    var database = openDatabase(
//      // Set the path to the database. Note: Using the `join` function from the
//      // `path` package is best practice to ensure the path is correctly
//      // constructed for each platform.
//      join(await getDatabasesPath(), 'nearlog_database.db'),
//      // When the database is first created, create a table to store dogs.
//      onCreate: (db, version) {
//        return db.execute(
//          "CREATE TABLE dogs(Uuid TEXT PRIMARY KEY, Date INTEGER, Duration INTEGER, MedDist INTEGER)",
//        );
//      },
//      // Set the version. This executes the onCreate function and provides a
//      // path to perform database upgrades and downgrades.
//      version: 1,
//    );
//  }

//  dbAddD(database) async { // DataBaseADDData
//    var db = await database;
//
//  }

  @override
  initState() {
    super.initState();
  }

  final controller = PageController(initialPage: 0);

  Widget build(BuildContext context) {
    return Scaffold(
        body: PageView(
          controller: controller,
          children: <Widget>[
            //            Container(
            //              child: ListenRoute(
            //                storage: ContactStorage(),
            //              )
            //            ),
            //            Container(
            //              child: BroadcastRoute()
            //            ),
            Container(
              child: BleRoute(
                storage: ContactStorage(),
              ),
            ),
            Container(
                height: 1000,
                child: Scaffold(
                  appBar: AppBar(
                    title: Text('Nearlog'),
                  ),
                  body: ListView(
                    children: <Widget>[
                      Center(
                        child: Text('Swipe left to see more info.'),
                      ),
                      //                    ListenRoute(
                      //                      storage: ContactStorage(),
                      //                    ),
                      StatusRoute(),
                      ReportRoute(),
                      HistoryRoute(storage: ContactStorage()),
                      SettingsRoute(),
                      QaaRoute(),
                    ],
                  ),
                )
            ),
          ],
        )
    );
  }
}
