//import 'package:flutter/material.dart';
//import 'package:uuid/uuid.dart';
//import 'package:flutter_blue/flutter_blue.dart';
//import 'package:shared_preferences/shared_preferences.dart';
////import 'history_route.dart';
//
//void main() => runApp(RootApp());
//
//class RootApp extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return MaterialApp(
//      title: 'Flutter Demo',
//      theme: ThemeData(
//        primarySwatch: Colors.blue,
//      ),
//      home: RootRoute(title: 'Flutter Demo Home Page'),
//    );
//  }
//}
//
//class RootRoute extends StatefulWidget {
//  RootRoute({Key key, this.title}) : super(key: key);
//  final String title;
//  @override
//  _RootRouteState createState() => _RootRouteState();
//}
//
//class _RootRouteState extends State<RootRoute> {
//  var uuid = new Uuid();
//  var _uuid = "not_set";
//  var _bleOk = false;
//  var _bleShowTextfield = TextEditingController();
//  FlutterBlue flutterBlue = FlutterBlue.instance;
//  Widget build(BuildContext context) {
//    return Scaffold(
//        appBar: AppBar(
//          title: Text('Nearlog'),
//        ),
//        body: Column(
//          //mainAxisAlignment: MainAxisAlignment.center,
//          children: <Widget>[
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                child: Text('Read'),
//                onPressed: () {
//                  _readUuid();
//                },
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                child: Text('Check BLE'),
//                onPressed: () {
//                  _check_ble_ok();
//                },
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                child: Text('Show BLE for 10 secs'),
//                onPressed: () {
//                  _bleShowTextfield.text = "Results:\n";
//                  _scan_ble(10);
//                },
//              ),
//            ),
//            TextField(
//              controller: _bleShowTextfield,
//              keyboardType: TextInputType.multiline,
//              maxLines: null,
//            ),
//            TextField(
//              decoration: const InputDecoration(
//                hintText: 'BLE Scan Duration',
//              ),
//
//            ),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                child: Text('GenSave'),
//                onPressed: () {
//                  _genSaveUuid();
//                },
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: Text('This device\'s UUID is:'),
//            ),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: Text('$_uuid'),
//            ),
//          ],
//        ));
//  }
//
//  _readUuid() async {
//    final prefs = await SharedPreferences.getInstance();
//    final key = 'uuid';
//    final value = prefs.getString(key) ?? 0;
//    setState(() { _uuid = value; });
//    print('read: $value');
//  }
//
//  _saveUuid() async {
//    final prefs = await SharedPreferences.getInstance();
//    final key = 'uuid';
//    final value = '$_uuid';
//    prefs.setString(key, value);
//    print('saved $value');
//    print('uuid is now $_uuid');
//  }
//
//  _genUuid() async {
//    var newValue = uuid.v4();
//    setState(() { _uuid = newValue; });
//  }
//
//  _genSaveUuid() async {
//    await _genUuid();
//    _saveUuid();
//  }
//
//  _check_ble_ok() async {
//    print('heelo');
////    if (flutterBlue.isAvailable()) {
////      _ble_ok = true;
////    }
////    else {
////      _ble_ok = false;
////    }
//  }
//
//  _scan_ble(seconds) async {
//    // Start scanning
//    flutterBlue.startScan(timeout: Duration(seconds: seconds));
//
//    // Listen to scan results
//    var subscription = flutterBlue.scanResults.listen((results) {
//      // do something with scan results
//      for (ScanResult r in results) {
//        var name = "Unknown";
//        if (r.device.name != "") {
//          name = r.device.name;
//        }
//        _bleShowTextfield.text = _bleShowTextfield.text + '${name} (${r.rssi})\n';
//      }
//    });
//
//    // Stop scanning
//    flutterBlue.stopScan();
//  }
//}
