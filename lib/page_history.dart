import 'package:flutter/material.dart';

import 'dart:async';
import 'dart:math';

import 'package:beacon_broadcast/beacon_broadcast.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_beacon/flutter_beacon.dart';

import 'lib_file.dart';

class HistoryRoute extends StatefulWidget {
  ContactStorage storage;

  HistoryRoute({Key key, @required this.storage}) : super(key: key);
  @override
  _HistoryRouteState createState() => _HistoryRouteState();
}

class _HistoryRouteState extends State<HistoryRoute>
    with WidgetsBindingObserver {
  var _contacts = '';

  reloadContacts() async {
    var cache = await widget.storage.readContact();
    setState(() {
      _contacts = cache;
    });
  }

  Widget build(BuildContext context) {
    return Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 200.0,
              child: Column(
                children: <Widget>[
                  if (_contacts != '')
                    SingleChildScrollView(
                      child: Text(
                        '$_contacts',
                      ),
                    )
                  else
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(),
                        Text('\nLoading History...'),
                      ],
                    )
                ],
              ),
            ),
            SingleChildScrollView(
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('⚠️ This app is work in progress. Some or all features may not be available or wrongly documented.',
                              style: Theme.of(context).textTheme.headline),
                        ]
                    )
                )
            )
          ],
        )
    );
  }

  @override
  bool get wantKeepAlive => true;
}
