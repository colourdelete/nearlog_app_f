import 'dart:async';
import 'dart:math';
import 'dart:ui';

import 'package:beacon_broadcast/beacon_broadcast.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_beacon/flutter_beacon.dart';

import 'lib_file.dart';

class BleRoute extends StatefulWidget {
  final ContactStorage storage;

  BleRoute({Key key, @required this.storage}) : super(key: key);

  @override
  _BleRouteState createState() => _BleRouteState();
}

class _BleRouteState extends State<BleRoute> with WidgetsBindingObserver {
  StreamController<BluetoothState> streamController = StreamController();
  StreamSubscription<BluetoothState> _streamBluetooth;
  StreamSubscription<RangingResult> _streamRanging;
  var _regionBeacons = <Region, List<Beacon>>{};
  var _beacons = <Beacon>[];
  var _beaconsTime = <String, int>{};
  var bleIdentifier = 'com.colourdelete.nearlog_app_f'; // BLE IDENTIFIER
  var bleProxUUID = '00a5f9d2-7253-4535-98f1-30de9fe1d311'; // DO NOT CHANGE; WILL NOT BE ABLE TO COMMUNICATE/LISTEN TO OTHER NEARLOG STUFF // BLE PROXimity UUID
  bool authorizationStatusOk = false;
  bool locationServiceEnabled = false;
  bool bluetoothEnabled = false;
  var _contacts = 'loading';

  var _ndidDict = {}; // Nearlog Device ID
  var _ndidList = [];

  var _uuid = "00a5f9d2-7253-4535-98f1-30de9fe1d311";
  var bleShowTextfield = TextEditingController();
  var _broadcastOk = "Unknown";
  var _broadcastAd = "Unknown";
  var _broadcastErr = "No Errors";
  var _bleBroadcastMajorId = 0;
  var _bleBroadcastMinorId = 0;
  var _transPower = -59;
  static const _bleBroadcastIdentifier = 'com.colourdelete.nearlog_app_deviceRegion';
//  static const _bleBroadcastLayout = BeaconBroadcast.ALTBEACON_LAYOUT;
  static const _bleBroadcastLayout = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24";
  // iBeacon layout: https://stackoverflow.com/questions/25027983/is-this-the-correct-layout-to-detect-ibeacons-with-altbeacons-android-beacon-li

  // https://stackoverflow.com/questions/26607423/detect-ibeacon-using-altbeacon-library
  // setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24")
  //
  static const _bleBroadcastMId = 0x004c;

  BeaconBroadcast beaconBroadcast = BeaconBroadcast();

//  BeaconStatus _isTransmissionSupported;
//  bool _isAdvertising = false;
  StreamSubscription<bool> _isAdvertisingSubscription;

  // Broadcast

  void bleBroadcastDispose() {
    super.dispose();
    if (_isAdvertisingSubscription != null) {
      _isAdvertisingSubscription.cancel();
    }
  }

  Future<void> bleBroadcastCheckOk() async {
    _broadcastOk = "Checking...";
    var transmissionSupportStatus = await beaconBroadcast.checkTransmissionSupported();
    switch (transmissionSupportStatus) {
      case BeaconStatus.SUPPORTED:
      // You're good to go, you can advertise as a beacon
        _broadcastOk = "Broadcasting Supported";
        break;
      case BeaconStatus.NOT_SUPPORTED_MIN_SDK:
      // Your Android system version is too low (min. is 21)
        _broadcastOk = "Broadcasting Not Supported (min SDK)";
        break;
      case BeaconStatus.NOT_SUPPORTED_BLE:
      // Your device doesn't support BLE
        _broadcastOk = "Broadcasting Not Supported (BLE not supported)";
        break;
      case BeaconStatus.NOT_SUPPORTED_CANNOT_GET_ADVERTISER:
      // Either your chipset or driver is incompatible
        _broadcastOk = "Broadcasting Not Supported (Advertiser fetch failed)";
        break;
    }
  }

  bleBroadcastCheckAd() async {
    _broadcastAd = "Checking...";
    var isAdvertising = beaconBroadcast.isAdvertising();
    switch (await isAdvertising) {
      case true:
        setState(() {
          _broadcastAd = "On/Broadcasting";
        });
        break;
      case false:
        setState(() {
          _broadcastAd = "Off/Not Broadcasting";
        });
        break;
      default:
        setState(() {
          _broadcastAd = "Unknown";
        });
        break;
    }
  }

  bleBroadcastErr(trace) async {
    _broadcastErr = "Error: $trace";
  }

  bleBroadcastStart() async {
    _broadcastErr = "No Errors";
    await beaconBroadcast.stop();
    await beaconBroadcast
        .setUUID(_uuid)
        .setMajorId(_bleBroadcastMajorId)
        .setMinorId(_bleBroadcastMinorId)
        .setTransmissionPower(_transPower)
        .setIdentifier(_bleBroadcastIdentifier)
        .setLayout(_bleBroadcastLayout)
        .setManufacturerId(_bleBroadcastMId)
        .start().catchError(bleBroadcastErr);
    await new Future.delayed(const Duration(seconds : 1));
    await bleBroadcastCheckAd();
  }

  bleBroadcastStop() async {
    _broadcastErr = "No Errors";
    await beaconBroadcast.stop();
    await new Future.delayed(const Duration(seconds : 1));
    await bleBroadcastCheckAd();
  }

  Future<void> bleBroadcastCheck() async {
    await bleBroadcastCheckOk();
    await new Future.delayed(const Duration(seconds : 1));
    await bleBroadcastCheckAd();
  }

  void initState() {
    super.initState();
    bleInitState();
  }

  Future<void> bleInitState() async {
    await genBleIDs();
    await bleBroadcastStart();
    bleBroadcastCheck();
    WidgetsBinding.instance.addObserver(this);
    await listeningState();
  }

  getNDID(beacon) {
    return '${beacon.major.toString()}-${beacon.minor.toString()}';
  }

  getNDIDRaw(major, minor) {
    return '$major-$minor';
  }

  reloadContacts() async {
    var cache = await widget.storage.readContact();
    setState(() {
      _contacts = cache;
    });
  }

  getDate() async {
    var now = new DateTime.now();
    return '${now.year}-${now.month}-${now.day}';
  }

  getEpoch() async {
    var now = new DateTime.now();
    return now.millisecondsSinceEpoch ~/ 1000;
  }

  clearContacts() async {
    await widget.storage.clearContact();
    reloadContacts();
  }

  saveContacts() async {
    await new Future.delayed(const Duration(seconds: 5));
    for (var i = 0; i < _ndidDict.length; i++) {
      var epoch = getEpoch();
      var key = _ndidDict.keys.toList()[i];
      var line = await _ndidDict[key];
      var lastTime = (line[2] + line[3]).toInt();
      var untouchedTime = (await epoch - lastTime);
      if (untouchedTime >= 20) {
        var duration = _ndidDict[key][3].round();
        await widget.storage.writeContact(
            '$key ${_ndidDict[key][0]} ${_ndidDict[key][1]} ${(duration / 60)
                .round()}');
        _ndidDict.remove(key);
      }
    }
  }

  getDuration(beacon) {
    var ndid = getNDID(beacon);
    var alreadyThere = _ndidDict.containsKey(ndid);
    if (alreadyThere) {
      return _ndidDict[ndid][3];
    }
    else {
      return 0;
    }
  }

  getMinDuration(beacon) {
    return (getDuration(beacon) / 60).round();
  }

  trySaveContacts(beacon) async {
    var ndid = getNDID(beacon);
    var prox = beacon.proximity.toString();
    var date = getDate();
    var alreadyThere = _ndidDict.containsKey(ndid);
    var epoch = await getEpoch();
    if (alreadyThere) {
      var startTime = _ndidDict[ndid][2];
      _ndidDict[ndid] = [_ndidDict[ndid][0], _ndidDict[ndid][1],
        startTime, epoch - startTime];
    }
    else {
      _ndidDict[ndid] = [await date, prox, epoch, epoch];
      _ndidList.add(ndid);
    }
    // date prox start_time, duration
//    print('$ndid ${_ndidDict[ndid]}');
    reloadContacts();

  }



  listeningState() async {
    print('Listening to bluetooth state');
    _streamBluetooth = flutterBeacon
        .bluetoothStateChanged()
        .listen((BluetoothState state) async {
      print('BluetoothState = $state');
      streamController.add(state);

      switch (state) {
        case BluetoothState.stateOn:
          initScanBeacon();
          break;
        case BluetoothState.stateOff:
          await pauseScanBeacon();
          await checkAllRequirements();
          break;
      }
    });
  }

  checkAllRequirements() async {
    final bluetoothState = await flutterBeacon.bluetoothState;
    final bluetoothEnabled = bluetoothState == BluetoothState.stateOn;
    final authorizationStatus = await flutterBeacon.authorizationStatus;
    final authorizationStatusOk =
        authorizationStatus == AuthorizationStatus.allowed ||
            authorizationStatus == AuthorizationStatus.always;
    final locationServiceEnabled =
    await flutterBeacon.checkLocationServicesIfEnabled;

    setState(() {
      this.authorizationStatusOk = authorizationStatusOk;
      this.locationServiceEnabled = locationServiceEnabled;
      this.bluetoothEnabled = bluetoothEnabled;
    });
  }

  static int currentTimeInSeconds() {
    var ms = (new DateTime.now()).millisecondsSinceEpoch;
    return (ms / 1000).round();
  }

  initScanBeacon() async {
    await flutterBeacon.initializeScanning;
    await checkAllRequirements();
    if (!authorizationStatusOk ||
        !locationServiceEnabled ||
        !bluetoothEnabled) {
      print('RETURNED, authorizationStatusOk=$authorizationStatusOk, '
          'locationServiceEnabled=$locationServiceEnabled, '
          'bluetoothEnabled=$bluetoothEnabled');
      return;
    }
    var regions = <Region>[
      Region(
        identifier: bleIdentifier,
        proximityUUID: bleProxUUID,
      ),
    ];

    if (_streamRanging != null) {
      if (_streamRanging.isPaused) {
        _streamRanging.resume();
        return;
      }
    }


    _streamRanging =
        flutterBeacon.ranging(regions).listen((RangingResult result) {

          if (result != null && mounted) {
            setState(() {
              _regionBeacons[result.region] = result.beacons;
              _regionBeacons.values.forEach((list) {
//                _beacons.addAll(list);
                if (list.isNotEmpty) {
                  var key = getNDID(list.toList()[0]);
                  if (!_beaconsTime.containsKey(key)) {
                    _beacons.addAll(list);
                    _beaconsTime[key] = currentTimeInSeconds();
                  }
//                  _beaconsTime[key] = currentTimeInSeconds();

                  // remove beacons not received more than 30 seconds
                  _beacons.forEach((b) {
                    var key_b = getNDID(b);
                    print(currentTimeInSeconds() - _beaconsTime[key_b]);
                    print('abc');
                    var beaconKey = findBeaconIdxByNDID(key_b, _beacons);
                    print(beaconKey);
                    if (currentTimeInSeconds() - _beaconsTime[key_b] > 30) {
                      print('Beacon $key_b/$beaconKey has timed out (undiscoverable for more than 30 sec).');
                      if (beaconKey != -1) {
                        _beacons.remove(beaconKey);
                        print('Removing $beaconKey from _beacons.');
                      }
                      else {
                        print('This error shouldn\'t happen! Skipped removing from _beacons because beacon doesn\'t exist in _beaacons.');
                      }
                      if (!_beaconsTime.containsKey(key_b)) {
                        print('Removing $key_b from _beaconsTime.');
                        _beaconsTime.remove(key_b);
                      }
                      else {
                        print('This error shouldn\'t happen! Skipped removing from _beaconsTime because beacon doesn\'t exist in _beaaconsTime.');
                      }
                    }
                  });
                }
              });
              _beacons.sort(_compareParameters);
            });
          }
        });
  }

  pauseScanBeacon() async {
    _streamRanging?.pause();
    if (_beacons.isNotEmpty) {
      setState(() {
        _beacons.clear();
      });
    }
  }

  int _compareParameters(Beacon a, Beacon b) {
    int compare = a.proximityUUID.compareTo(b.proximityUUID);

    if (compare == 0) {
      compare = a.major.compareTo(b.major);
    }

    if (compare == 0) {
      compare = b.minor.compareTo(a.minor);
    }

    return compare;
  }

  int findBeaconIdxByNDID(String NDID, List<Beacon> list) {
    list.forEach((beacon) {
      if (getNDID(beacon) == NDID) {
        return list.indexOf(beacon);
      };
    });
    return -1;
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    print('AppLifecycleState = $state');
    if (state == AppLifecycleState.resumed) {
      if (_streamBluetooth != null && _streamBluetooth.isPaused) {
        _streamBluetooth.resume();
      }
      await checkAllRequirements();
      if (authorizationStatusOk && locationServiceEnabled && bluetoothEnabled) {
        await initScanBeacon();
      } else {
        await pauseScanBeacon();
        await checkAllRequirements();
      }
    } else if (state == AppLifecycleState.paused) {
      _streamBluetooth?.pause();
    }
  }

  genRandom() async {
    var rng = new Random.secure();
    return rng.nextInt(65534) + 1; // 0 is reserved for testing
  }

  genBleIDs() async {
    var major = await genRandom();
    var minor = await genRandom();
    setState(() {
      _bleBroadcastMajorId = major;
      _bleBroadcastMinorId = minor;
    });
  }

  genBLEIDsApply() async {
    await genBleIDs();
    bleBroadcastStart();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    streamController?.close();
    _streamRanging?.cancel();
    _streamBluetooth?.cancel();
    flutterBeacon.close;

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nearlog'),
        actions: <Widget>[
          Center(
              child: Row(
                children: <Widget>[
                  if (!authorizationStatusOk)
                    IconButton(
                        icon: new Icon(Icons.portable_wifi_off),
                        color: Colors.red,
                        onPressed: () async {
                          await flutterBeacon.requestAuthorization;
                        }
                    )
                  else
                    IconButton(
                        icon: new Icon(Icons.wifi_tethering),
                        color: Colors.green,
                        onPressed: () async {
                          await flutterBeacon.requestAuthorization;
                        }
                    ),
                ],
              )
          ),
          Center(
              child: Row(
                children: <Widget>[
                  if (!locationServiceEnabled)
                    IconButton(
                        icon: new Icon(Icons.location_off),
                        color: Colors.red,
                        onPressed: () async {
                          await flutterBeacon.openLocationSettings;
                        }
                    )
                  else
                    IconButton(
                        icon: new Icon(Icons.location_on),
                        color: Colors.green,
                        onPressed: () async {
                          await flutterBeacon.openLocationSettings;
                        }
                    ),
                ],
              )
          ),
          Center(
              child: Row(
                children: <Widget>[
                  if (!bluetoothEnabled)
                    IconButton(
                        icon: new Icon(Icons.bluetooth_disabled),
                        color: Colors.red,
                        onPressed: () async {
                          await flutterBeacon.openBluetoothSettings;
                        }
                    )
                  else
                    IconButton(
                        icon: new Icon(Icons.bluetooth),
                        color: Colors.green,
                        onPressed: () async {
                          await flutterBeacon.openBluetoothSettings;
                        }
                    ),
                ],
              )
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          Center(
            child: Column(
                children: <Widget>[
                  Center(
                    child: Text('My NDID:',
                        style: Theme
                            .of(context)
                            .textTheme
                            .headline5),
                  ),
                  Center(
                    child: Text('${getNDIDRaw(
                        _bleBroadcastMajorId, _bleBroadcastMinorId)}',
                        style: Theme
                            .of(context)
                            .textTheme
                            .headline5),
                  ),
                  Center(
                    child: RaisedButton(
                      child: Text('Gen NDID'),
                      onPressed: () {
                        genBLEIDsApply();
                      },
                    ),
                  ),
                ]
            ),
          ),
          if (authorizationStatusOk && locationServiceEnabled &&
              bluetoothEnabled)
            SizedBox(
              height: 500.0,
              child: Center(
                child: _beacons == null || _beacons.isEmpty
                    ? Center(child: CircularProgressIndicator())
                    : ListView(
                  children: ListTile.divideTiles(
                      context: context,
                      tiles: _beacons.map((beacon) {
                        trySaveContacts(beacon);
                        saveContacts();
                        return ListTile(
                            title: Text(
                              getNDID(beacon),
                            ),
                            subtitle: new Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      if (_beaconsTime[getNDID(beacon)] > 2)
                                      Text(
                                        'Timed out',
                                        style: TextStyle(
                                          color: Colors.red,
                                        ),
                                      ),
                                      if (_beaconsTime[getNDID(beacon)] <= 0)
                                        Text(
                                          'Not timed out',
                                          style: TextStyle(
                                            color: Colors.green,
                                          ),
                                        ),
                                      Flexible(
                                          child: Text(
                                              'Duration: ${getMinDuration(
                                                  beacon)} min',
                                              style: TextStyle(fontSize: 13.0)),
                                          flex: 1,
                                          fit: FlexFit.tight
                                      ),
                                      Flexible(
                                          child: Text(
                                              'Accuracy: ${beacon
                                                  .accuracy}m\nRSSI: ${beacon
                                                  .rssi}',
                                              style: TextStyle(fontSize: 13.0)),
                                          flex: 1,
                                          fit: FlexFit.tight
                                      ),
                                    ],
                                  ),
                                ]
                            )
                        );
                      }
                      )
                  ).toList(),
                ),
              ),
            )
          else
            SizedBox(
              height: 200.0,
              child: Center(
                  child: Text(
                      'Tap the red icon(s) on the top left and accept the permissions to scan.')
              ),
            ),
        Center(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      if (_broadcastOk == 'Broadcasting Supported')
                        Text('$_broadcastOk',
                          style: TextStyle(color: Colors.green),
                        )
                      else
                        if (_broadcastOk.contains('Broadcasting Not Supported'))
                          Text('$_broadcastOk',
                            style: TextStyle(color: Colors.red),
                          )
                        else
                          Text('$_broadcastOk',
                            style: TextStyle(color: Colors.grey),
                          ),
                      Container(height: 16.0),
                      if (_broadcastAd == 'On/Broadcasting')
                        Text('$_broadcastAd',
                          style: TextStyle(color: Colors.green),
                        )
                      else
                        if (_broadcastAd == 'Off/Not Broadcasting')
                          Text('$_broadcastAd',
                            style: TextStyle(color: Colors.red),
                          )
                        else
                          Text('$_broadcastAd',
                            style: TextStyle(color: Colors.grey),
                          ),
                      Container(height: 16.0),
                      if (_broadcastErr.contains('No Error'))
                        Text('$_broadcastErr',
                          style: TextStyle(color: Colors.green),
                        )
                      else
                        Text('$_broadcastErr',
                          style: TextStyle(color: Colors.red),
                        ),
                      Container(height: 16.0),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        ExpandablePanel(
          header: Text(
            'Debug Data',
          ),
          expanded: Column(
              children: <Widget>[
                Text('Listen',
                    style: Theme
                        .of(context)
                        .textTheme
                        .headline5),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                    onPressed: reloadContacts,
                    child: Text('Read'),
                  ),
                    RaisedButton(
                    onPressed: clearContacts,
                    child: Text('Clear'),
                  ),
                  ],
                ),
                SizedBox(
                  height: 200.0,
                  child: SingleChildScrollView(
                    child: Text(
                      '$_contacts',
                    ),
                  ),
                ),
                Text('Broadcast',
                    style: Theme
                        .of(context)
                        .textTheme
                        .headline5),
                Center(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: () async {
                            await bleBroadcastCheckOk();
                          },
                          child: Text('Check Support'),
                        ),
                        RaisedButton(
                          onPressed: () async {
                            await bleBroadcastCheckAd();
                          },
                          child: Text('Check State'),
                        ),
                      ]
                  ),
                ),
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        onPressed: () {
                          bleBroadcastStart();
                        },
                        child: Text('Re/start'),
                      ),
                      RaisedButton(
                        onPressed: () {
                          bleBroadcastStop();
                        },
                        child: Text('Stop'),
                      ),
                    ],
                  ),
                ),
                Text('UUID: $_uuid'),
                Text('Major id: $_bleBroadcastMajorId'),
                Text('Minor id: $_bleBroadcastMinorId'),
                Text('Tx Power: $_transPower'),
                Text('Identifier: $_bleBroadcastIdentifier'),
                Text('Layout: $_bleBroadcastLayout'),
                Text('Manufacturer Id: $_bleBroadcastMId'),
              ]
          ),
//          tapHeaderToExpand: true,
        ),
      ]
      )
    );
  }

  bool get wantKeepAlive => true;
}