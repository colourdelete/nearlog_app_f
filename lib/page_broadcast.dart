//import 'dart:async';
//
//import 'package:beacon_broadcast/beacon_broadcast.dart';
//import 'package:flutter/material.dart';
//import 'package:shared_preferences/shared_preferences.dart';
//import 'package:uuid/uuid.dart';
//
//class BroadcastRoute extends StatefulWidget {
//  BroadcastRoute({Key key, this.title}) : super(key: key);
//  final String title;
//
//  @override
//  _BroadcastRouteState createState() => _BroadcastRouteState();
//}
//
//class _BroadcastRouteState extends State<BroadcastRoute> with WidgetsBindingObserver {
//  var uuid = new Uuid();
//  var _uuid = "00a5f9d2-7253-4535-98f1-30de9fe1d311";
//  var bleShowTextfield = TextEditingController();
//  var _broadcastOk = "Unknown";
//  var _broadcastAd = "Unknown";
//  var _broadcastErr = "No Errors";
//  static const _bleBroadcastMajorId = 1;
//  static const _bleBroadcastMinorId = 100;
//  static const _transPower = -59;
//  static const _bleBroadcastIdentifier = 'com.colourdelete.nearlog_app_deviceRegion';
////  static const _bleBroadcastLayout = BeaconBroadcast.ALTBEACON_LAYOUT;
//   static const _bleBroadcastLayout = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24";
//  // iBeacon layout: https://stackoverflow.com/questions/25027983/is-this-the-correct-layout-to-detect-ibeacons-with-altbeacons-android-beacon-li
//  static const _bleBroadcastMId = 0x004c;
//
//  BeaconBroadcast beaconBroadcast = BeaconBroadcast();
//  BeaconStatus _isTransmissionSupported;
//  bool _isAdvertising = false;
//  StreamSubscription<bool> _isAdvertisingSubscription;
//
//  // Broadcast
//
//  void bleBroadcastDispose() {
//    super.dispose();
//    if (_isAdvertisingSubscription != null) {
//      _isAdvertisingSubscription.cancel();
//    }
//  }
//
//  Future<void> bleBroadcastCheckOk() async {
//    _broadcastOk = "Checking...";
//    var transmissionSupportStatus = await beaconBroadcast.checkTransmissionSupported();
//    switch (transmissionSupportStatus) {
//      case BeaconStatus.SUPPORTED:
//      // You're good to go, you can advertise as a beacon
//        _broadcastOk = "Broadcasting Supported";
//        break;
//      case BeaconStatus.NOT_SUPPORTED_MIN_SDK:
//      // Your Android system version is too low (min. is 21)
//        _broadcastOk = "Broadcasting Not Supported (min SDK)";
//        break;
//      case BeaconStatus.NOT_SUPPORTED_BLE:
//      // Your device doesn't support BLE
//        _broadcastOk = "Broadcasting Not Supported (BLE not supported)";
//        break;
//      case BeaconStatus.NOT_SUPPORTED_CANNOT_GET_ADVERTISER:
//      // Either your chipset or driver is incompatible
//        _broadcastOk = "Broadcasting Not Supported (Advertiser fetch failed)";
//        break;
//    }
//  }
//
//  bleBroadcastCheckAd() async {
//    _broadcastAd = "Checking...";
//    var isAdvertising = beaconBroadcast.isAdvertising();
//    switch (await isAdvertising) {
//      case true:
//        setState(() {
//          _broadcastAd = "On/Broadcasting";
//        });
//        break;
//      case false:
//        setState(() {
//          _broadcastAd = "Off/Not Broadcasting";
//        });
//        break;
//      default:
//        setState(() {
//          _broadcastAd = "Unknown";
//        });
//        break;
//    }
//  }
//
//  bleBroadcastErr(trace) async {
//    _broadcastErr = "Error: $trace";
//  }
//
//  bleBroadcastStart() async {
//    _broadcastErr = "No Errors";
//    await beaconBroadcast.stop();
//    await beaconBroadcast
//        .setUUID(_uuid)
//        .setMajorId(_bleBroadcastMajorId)
//        .setMinorId(_bleBroadcastMinorId)
//        .setTransmissionPower(_transPower)
//        .setIdentifier(_bleBroadcastIdentifier)
//        .setLayout(_bleBroadcastLayout)
//        .setManufacturerId(_bleBroadcastMId)
//        .start().catchError(bleBroadcastErr);
//    await new Future.delayed(const Duration(seconds : 1));
//    await bleBroadcastCheckAd();
//
////    var transmissionSupportStatus = await beaconBroadcast.checkTransmissionSupported();
////    switch (transmissionSupportStatus) {
////      case BeaconStatus.SUPPORTED:
////      // You're good to go, you can advertise as a beacon
////        print("::: supported");
////        break;
////      case BeaconStatus.NOT_SUPPORTED_MIN_SDK:
////      // Your Android system version is too low (min. is 21)
////        print("::: MIN SDK");
////        break;
////      case BeaconStatus.NOT_SUPPORTED_BLE:
////      // Your device doesn't support BLE
////        print("::: BLE not supported");
////        break;
////      case BeaconStatus.NOT_SUPPORTED_CANNOT_GET_ADVERTISER:
////      // Either your chipset or driver is incompatible
////        print("::: imcomplatible");
////        break;
////    }
////    beaconBroadcast.stop();
////    await beaconBroadcast
////        .setUUID('00a5f9d2-7253-4535-98f1-30de9fe1d311')
////        .setMajorId(55)
////        .setMinorId(66)
////        .start();
////    await bleBroadcastCheckAd();
//  }
//
//  bleBroadcastStop() async {
//    _broadcastErr = "No Errors";
//    await beaconBroadcast.stop();
//    await new Future.delayed(const Duration(seconds : 1));
//    await bleBroadcastCheckAd();
//  }
//
//  Future<void> bleBroadcastInitState() async {
//    super.initState();
////    await bleBroadcastCheckOk();
////    bleBroadcastCheckAd();
////    await bleBroadcastStart();
////    await bleBroadcastCheckAd();
////    beaconBroadcast
////        .checkTransmissionSupported()
////        .then((isTransmissionSupported) {
////      bleBroadcastCheckAd();
//////      setState(() {
//////        _isTransmissionSupported = isTransmissionSupported;
//////      });
////    });
////    _isAdvertisingSubscription =
////      beaconBroadcast.getAdvertisingStateChange().listen((isAdvertising) {
////        setState(() {
////          _isAdvertising = isAdvertising;
////        }
////        );
////      }
////      );
//  }
//
//  _readUuid() async {
//    final prefs = await SharedPreferences.getInstance();
//    final key = 'uuid';
//    final value = prefs.getString(key) ?? 0;
//    setState(() {
//      _uuid = value;
//    });
//    print('read: $value');
//  }
//
//  _saveUuid() async {
//    final prefs = await SharedPreferences.getInstance();
//    final key = 'uuid';
//    final value = '$_uuid';
//    prefs.setString(key, value);
//    print('saved $value');
//    print('uuid is now $_uuid');
//  }
//
//  _genUuid() async {
//    var newValue = uuid.v4();
//    setState(() {
//      _uuid = newValue;
//    });
//  }
//
//  _genSaveUuid() async {
//    await _genUuid();
//    _saveUuid();
//  }
//
//  Widget build(BuildContext context) {
//    return Scaffold(
//        appBar: AppBar(
//          title: Text('Broadcast'),
//        ),
//        body: Center(
//          child: Column(
//            //mainAxisAlignment: MainAxisAlignment.center,
//            children: <Widget>[
//              SingleChildScrollView(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    mainAxisSize: MainAxisSize.min,
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    children: <Widget>[
//                      Text('$_broadcastOk',
//                          style: Theme.of(context).textTheme.subhead),
//                      Container(height: 16.0),
//                      Text('$_broadcastAd',
//                          style: Theme.of(context).textTheme.subhead),
//                      Container(height: 16.0),
//                      Text('$_broadcastErr',
//                          style: Theme
//                              .of(context)
//                              .textTheme
//                              .subhead),
//                      Container(height: 16.0),
//                      Center(
//                        child: Row(
//                          children: <Widget>[
//                            RaisedButton(
//                              onPressed: () async {
//                                await bleBroadcastCheckOk();
//                              },
//                              child: Text('Check Support'),
//                            ),
//                            RaisedButton(
//                              onPressed: () async {
//                                await bleBroadcastCheckAd();
//                              },
//                              child: Text('Check State'),
//                            ),
//                          ]
//                        ),
//                      ),
//                      Center(
//                        child: Row(
//                          children: <Widget>[
//                            RaisedButton(
//                              onPressed: () {
//                                bleBroadcastStart();
//                              },
//                              child: Text('Re/start'),
//                            ),
//                            RaisedButton(
//                              onPressed: () {
//                                bleBroadcastStop();
//                              },
//                              child: Text('Stop'),
//                            ),
//                          ],
//                        ),
//                      ),
//                      Text('Beacon Data',
//                          style: Theme.of(context).textTheme.headline),
//                      Text('UUID: $_uuid'),
//                      Text('Major id: $_bleBroadcastMajorId'),
//                      Text('Minor id: $_bleBroadcastMinorId'),
//                      Text('Tx Power: $_transPower'),
//                      Text('Identifier: $_bleBroadcastIdentifier'),
//                      Text('Layout: $_bleBroadcastLayout'),
//                      Text('Manufacturer Id: $_bleBroadcastMId'),
//                      Padding(
//                        padding: const EdgeInsets.all(8.0),
//                        child: RaisedButton(
//                          child: Text('Read'),
//                          onPressed: () {
//                            _readUuid();
//                          },
//                        ),
//                      ),
////                      Padding(
////                        padding: const EdgeInsets.all(8.0),
////                        child: RaisedButton(
////                          child: Text('GenSave'),
////                          onPressed: () {
////                            _genSaveUuid();
////                          },
////                        ),
////                      ),
//                    ],
//                  ),
//                ),
//              ),
//            ],
//          ),
//        )
//    );
//  }
//}
