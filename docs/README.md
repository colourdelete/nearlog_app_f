---
home: true
heroImage: /icon.svg
features:
- title: Anonymous
  details: Nearlog traces contacts anonymously. Nearlog collects no identifiable user data.
- title: Open source
  details: 
- title: Simple
  details:
footer: Nearlog by Colourdelete
---

![Pipeline Status](https://gitlab.com/colourdelete/nearlog_app_f/badges/master/pipeline.svg?style=flat-square "Pipeline Status")

⚠️This website is for the Nearlog Flutter App, not Nearlog (the service). In other words, this website is about the app itself, but not the whole system.
